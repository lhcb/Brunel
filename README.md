# General information

Brunel wss the the LHCb reconstruction programme for Run1 and Run2. Specific documentation is available at http://lhcbdoc.web.cern.ch/lhcbdoc/brunel/ 

This project contains several officialy maintained branches, with names such as `XXX-patches`.
They are all protected, meaning that code cannot be pushed into them directly but only through merge requests (MRs).
This helps with the validation of code prior to making it available in the official branches for future releases.

Note that the `master` branch, though present, is not used for this project.

## Available supported branches

- `run2-patches` branch: new developments and updates targeting runs 1+2 reprocessing. Builds on current supported platforms against latest version of Gaudi

- `2018-patches` branch: for patches to Brunel (`Reco18`) in 2018 simulation workflows. Builds with gcc62 on centos7

- `2017-patches` branch: for patches to Brunel (`Reco17`) in 2017 simulation workflows. Builds with gcc62 on centos7

- `2016-patches` branch: for patches to Brunel (`Reco16`) in 2016 simulation workflows. Builds with gcc49 on slc6

- `reco15-patches` branch: for patches to Brunel (`Reco15`) in 2015 simulation workflows. Builds with gcc49 on slc6

- `reco14-patches` branch: for patches to Brunel (`Reco14`) in run 1 simulation workflows. Builds with gcc46 on slc5. Requires CMT


## Where to commit code to

- Bug fixes specific to a given processing should be committed to the corresponding `XXX-patches` branch.

- Any changes or fixes for a future Run 1 and Run 2 re-reconstruction should go to the `run2-patches` branch.
  Fixes also relevant to specific `XXX-patches` branches should be flagged as such, they will  be propagated by the applications managers. 

In doubt, please get in touch before creating a MR.
