###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import EventSelector
from Configurables import Brunel

Brunel().DataType = "2010"
Brunel().DatasetName = "bigEvents-2010"

from GaudiConf.IOHelper import IOHelper
# file of selected large events
IOHelper("MDF").inputFiles([
    "$QMTESTFILESROOT/data/069669_2ev_bug65441.raw",
    "$QMTESTFILESROOT/data/070122_1ev_large.raw",
    "$QMTESTFILESROOT/data/071491_0000000091_evt35027_bug67364.raw"
])
