###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Brunel
from PRConfig import TestFileDB

TestFileDB.test_file_db['2015_pbpb_raw_full'].run(configurable=Brunel())

Brunel().EvtMax = 16
Brunel().DatasetName = "2015pbpb"
Brunel().Monitors = ["FPE"]

Brunel().Hlt1FilterCode = ""
Brunel().Hlt2FilterCode = "HLT_PASS_RE('Hlt2BBPassThroughDecision')"
