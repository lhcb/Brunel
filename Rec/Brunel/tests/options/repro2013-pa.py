###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = [
    "xmlcatalog_file:MyCatalog.xml",
    "xmlcatalog_file:$BRUNELROOT/job/NewCatalog.xml"
]

from Configurables import Brunel
Brunel().DataType = "2013"
Brunel().InputType = "DST"
file = "$QMTESTFILESROOT/data/136237_10ev_bug_svn164539.dst"
EventSelector().Input.append(file)
Brunel().SplitRawEventInput = 2.0  # Stripping20
# Suppress GECs for pA data
#importOptions("$APPCONFIGOPTS/Brunel/pA-GECs.py")
Brunel().SpecialData += ["pA"]
