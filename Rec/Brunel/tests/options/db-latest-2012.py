###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB, DDDBConf

dataType = "2012"

DDDBConf(DataType=dataType)

CondDB(IgnoreHeartBeat = True,
       EnableRunStampCheck = False)

CondDB().Tags.update({'DDDB': 'dt-'+dataType,
                      'LHCBCOND': 'dt-'+dataType})
