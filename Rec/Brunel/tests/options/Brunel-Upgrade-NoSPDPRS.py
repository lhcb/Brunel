###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Brunel without SPD/PRS
##############################################################################

from Brunel.Configuration import *

from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True

#if "Calo_NoSPDPRS" not in CondDB().AllLocalTagsByDataType:
#    CondDB().AllLocalTagsByDataType += ["Calo_NoSPDPRS"]

from Configurables import Brunel
for det in ["Spd", "Prs"]:
    if det in Brunel().Detectors:
        Brunel().Detectors.remove(det)

Brunel().DataType = "Upgrade"
