###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Pathological events from 2012 that caused huge processing times in Stripping21.
from Gaudi.Configuration import FileCatalog
from Configurables import Brunel

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = ["xmlcatalog_file:MyCatalog.xml"]

#-- Use latest 2012 database tags for real data
Brunel().DataType = "2012"
Brunel().InputType = "DST"

#-- Workaround for removal of concat_alternatives in KeyValue
from Configurables import Rich__Future__RawBankDecoder
richDecoder = Rich__Future__RawBankDecoder("RichFutureDecode")
richDecoder.RawEventLocation = "/Event/Rich/RawEvent"

#-- Twelve events with huge processing times in Reco14/Stripping21 draft.
from GaudiConf.IOHelper import IOHelper
IOHelper().inputFiles([
    "DATAFILE='$QMTESTFILESROOT/data/12Events_Stripping21Reco14_SlowEvents.dst'"
])
