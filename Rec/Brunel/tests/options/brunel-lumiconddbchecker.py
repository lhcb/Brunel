#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
import os
from Gaudi.Configuration import *

#--- switch on xml summary
from Configurables import LHCbApp
from Configurables import CondDB

#-- set explicit CondDB tag
LHCbApp().CondDBtag = 'head-20110131'

#--- determine application to run
from Configurables import LumiAlgsConf, LumiCheckCondDB
from Configurables import GaudiSequencer

# standard sequence from configurable
sequence = GaudiSequencer("CheckDB")

# normalization of BeamCrossing
seqMembers = []
seqMembers.append(LumiCheckCondDB('CheckCondDB'))
from Configurables import GetLumiParameters
ToolSvc().addTool(GetLumiParameters,
                  "lumiDatabaseTool")  # This is a public tool
ToolSvc().lumiDatabaseTool.UseOnline = True
sequence.Members = seqMembers
sequence.MeasureTime = True
sequence.ModeOR = False
sequence.ShortCircuit = True
sequence.IgnoreFilterPassed = False

#-- main
ApplicationMgr(
    TopAlg=[
        GaudiSequencer("CheckDB"),
    ], HistogramPersistency='NONE')

#-- Test input not needed
files = []

#-- input options
ApplicationMgr().EvtMax = -1
EventSelector(OutputLevel=INFO, PrintFreq=1000, FirstEvent=1, Input=files)
