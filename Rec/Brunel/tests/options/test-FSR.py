###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Test, will read teh FSR from a DST file
from Gaudi.Configuration import *
from Configurables import EventSelector, LHCbApp, GaudiSequencer
from LumiAlgs.Configuration import *

#Select DST file
EventSelector().Input = [
    "DATAFILE='PFN:bigEvents-2010.dst' TYP='POOL_ROOTTREE' OPT='READ'"
]

# Run LumiTest
#importOptions("$LUMIALGSROOT/job/LumiAlgsConfTest.py")

#select correct data format
LumiAlgsConf().InputType = 'DST'
LumiAlgsConf().DataType = '2009'
#print verbose output
LumiAlgsConf().OutputLevel = 1

lSeq = GaudiSequencer("LumiSequence")

LumiAlgsConf().LumiSequencer = lSeq

lSeq.OutputLevel = 1

LHCbApp().EvtMax = 100

ApplicationMgr().TopAlg += [lSeq]
ApplicationMgr().OutputLevel = 1
