###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper, RegexpReplacer
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

preprocessor = LHCbPreprocessor + \
  RegexpReplacer( when = "INFO Data source:", orig=r'Brunel-[a-f0-9]{1,8}x', repl=r'Brunel' ) + \
  RegexpReplacer( when = "INFO Writing ROOT histograms to:", orig=r'Brunel-[a-f0-9]{1,8}x', repl=r'Brunel' ) + \
  LineSkipper(["IODataManager       ERROR Referring to existing dataset"]) + \
  LineSkipper(["MagneticFieldSvc     INFO Opened magnetic field file"]) + \
  LineSkipper(["Memory for the event exceeds 3*sigma"]) + \
  LineSkipper(["Mean 'delta-memory' exceeds 3*sigma"]) + \
  LineSkipper(["BrunelInit.Brun...SUCCESS Exceptions/Errors/Warnings/Infos Statistics : 0/0/"]) + \
  LineSkipper(["| AfterMagnetRegion/"]) + \
  LineSkipper(["| MagnetRegion/"]) +\
  LineSkipper(["| BeforeMagnetRegion/"]) +\
  LineSkipper(["| DownstreamRegion/"]) +\
  LineSkipper(["MD5 sum:"])
