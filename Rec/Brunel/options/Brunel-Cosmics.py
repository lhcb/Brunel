###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Brunel on 2008 cosmic data with default 2008 geometry
# Syntax is:
#   gaudirun.py Brunel-Cosmics.py 2008-Cosmic-Data.py
##############################################################################

from Configurables import Brunel

# Set the special data options
Brunel().SpecialData = ["fieldOff", "cosmics"]

##############################################################################
# I/O datasets are defined in a separate file, see examples 2008-Cosmic-Data.py
##############################################################################
