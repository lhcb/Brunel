###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import appendPostConfigAction


def myOTrec():
    # disable OT cuts against late hits
    from Configurables import TrackSys
    from Gaudi.Configuration import DEBUG

    TrackSys().ExpertTracking = ["disableOTTimeWindow"]

    # this removes TimeWindow cut in OTRawBankDecoder
    from Configurables import (Tf__OTHitCreator)
    othc = Tf__OTHitCreator("OTHitCreator")
    othc.TMax = 999

    from Configurables import PatForward, PatForwardTool

    pf1 = PatForward("PatForwardHLT1")
    pf1.addTool(PatForwardTool)
    pf1.PatForwardTool.MaxOTDrift = 991.
    pf1.PatForwardTool.MinOTDrift = -991.
    pf1.PatForwardTool.UseProperMomentumEstimate = False
    pf1.PatForwardTool.UseMomentumEstimate = False
    pf1.PatForwardTool.NNBeforeXFit = False
    pf1.PatForwardTool.NNAfterStereoFit = False
    pf1.PatForwardTool.BetaLoop = True
    pf1.PatForwardTool.BetaLoopIncrement = 0.05
    pf1.PatForwardTool.BetaLoopMaxMass = 3727

    pf = PatForward("PatForwardHLT2")
    pf.addTool(PatForwardTool)
    pf.PatForwardTool.MaxOTDrift = 992.
    pf.PatForwardTool.MinOTDrift = -992.
    pf.PatForwardTool.NNBeforeXFit = False
    pf.PatForwardTool.NNAfterStereoFit = False
    pf.PatForwardTool.UseProperMomentumEstimate = False
    pf.PatForwardTool.UseMomentumEstimate = False
    pf.PatForwardTool.BetaLoop = True
    pf.PatForwardTool.BetaLoopIncrement = 0.05
    pf.PatForwardTool.BetaLoopMaxMass = 3727


appendPostConfigAction(myOTrec)
