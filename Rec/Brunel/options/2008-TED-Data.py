from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Special options for processing TED data taken in September 2008
# Details of these datasets are at https://lbtwiki.cern.ch/bin/view/Computing/VeryFirstData

from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp

if Brunel().getProp("InputType").upper() != "MDF":
    print(
        "********************************************************************************"
    )
    print(
        "The 2008-TED-Files.py file requires Brunel be set up to process MDF data"
    )
    print(
        "********************************************************************************"
    )
    import sys
    sys.exit()

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = [
    "xmlcatalog_file:MyCatalog.xml",
    "xmlcatalog_file:$BRUNELROOT/job/NewCatalog.xml"
]

# Default output files names are set up using value Brunel().DatasetName property
Brunel().DatasetName = "SeptTEDData"

# LHCbApp().DDDBtag   = "default"
# LHCbApp().CondDBtag = "default"

# Friday night with the velo - prev2
from PRConfig import TestFileDB
TestFileDB.test_file_db['2008_TED'].run(withDB=False)

# EventSelector().Input = [
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32474/032474_0000081642.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32476/032476_0000081643.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32477/032477_0000081644.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32479/032479_0000081647.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32481/032481_0000081648.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32484/032484_0000081651.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32493/032493_0000081660.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32498/032498_0000081699.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32500/032500_0000081701.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32501/032501_0000081702.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32576/032576_0000081818.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32578/032578_0000081820.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32580/032580_0000081822.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32581/032581_0000081823.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'",
#        "DATAFILE='mdf:root://castorlhcb.cern.ch//castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32583/032583_0000081825.raw?svcClass=lhcbtape'  SVC='LHCb::MDFSelector'"
#        ]


# Above files require following special options for Velo
def setPrev2():
    from Configurables import (DecodeVeloRawBuffer, UpdateManagerSvc)
    DecodeVeloRawBuffer('createBothVeloClusters').ForceBankVersion = 3
    DecodeVeloRawBuffer('createBothVeloClusters').RawEventLocations = [
        'Prev2/DAQ/RawEvent'
    ]
    UpdateManagerSvc().ConditionsOverride += [
        "Conditions/Online/Velo/MotionSystem := double ResolPosRC =-29. ; double ResolPosLA = 29. ;"
    ]


# These are needed as from run 32476, not in 32474 used in the test.
#    from Configurables import (RawBankToSTClusterAlg, RawBankToSTLiteClusterAlg)
#    RawBankToSTClusterAlg('createTTClusters').RawEventLocations = "/Event/Prev2/DAQ/RawEvent"
#    RawBankToSTLiteClusterAlg('createTTLiteClusters').RawEventLocations = "/Event/Prev2/DAQ/RawEvent"

appendPostConfigAction(setPrev2)

# Redefine defaults by uncommenting one or more of options below

# Monitoring histograms
#HistogramPersistencySvc().OutputFile = "SomeFile.root"

#-- Dst or rDst file
#OutputStream("DstWriter").Output = "DATAFILE='PFN:someFile.dst' TYP='POOL_ROOTTREE' OPT='REC'"
#OutputStream("DstWriter").Output = "DATAFILE='PFN:someFile.rdst' TYP='POOL_ROOTTREE' OPT='REC'"
