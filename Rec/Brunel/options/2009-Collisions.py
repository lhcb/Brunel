###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Example 2009 collisions options for Brunel

# Syntax is:
#   gaudirun.py 2009-Collisions.py
#
from GaudiKernel.ProcessJobOptions import importOptions
importOptions("$APPCONFIGOPTS/Brunel/veloOpen.py")

from Gaudi.Configuration import FileCatalog, EventSelector
from Configurables import Brunel

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = ["xmlcatalog_file:MyCatalog.xml"]

#-- Use latest 2009 database tags for real data
Brunel().DataType = "2009"

# First collisions with magnet off
#importOptions("$APPCONFIGOPTS/Brunel/moff.py")
#EventSelector().Input = [
#     "DATAFILE='root:/castor/cern.ch/grid/lhcb/data/2009/RAW/FULL/LHCb/BEAM1/62558/062558_0000000001.raw' SVC='LHCb::MDFSelector'"
#    ]

# Collisions on 6th December with magnet on and all detectors in readout
from PRConfig import TestFileDB
TestFileDB.test_file_db['2009_raw'].run(withDB=False)
