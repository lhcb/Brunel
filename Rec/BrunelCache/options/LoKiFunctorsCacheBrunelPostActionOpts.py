#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  helper configuration file needed for autogeneration of functors from
#  Stripping code
#
#  @author Vanya BELYAEV Ivan.Belayev@itep.ru
#  @date 2015-02-13
# =============================================================================


# =============================================================================
## post-config action  :
#   - enforce initialization of DoD-algorithms
#   - (remove some of them)
#   - reduce a bit the printout
#
def post_action_for_cpp():
    """
    Post-configh action to enforce initialization of DoD-algorithms
    """

    from Gaudi.Configuration import log, ERROR
    from Configurables import ApplicationMgr
    app = ApplicationMgr(OutputLevel=ERROR)
    app.EvtMax = 0
    app.EvtSel = 'NONE'

    from Configurables import LHCb__ParticlePropertySvc as PPSvc
    from Configurables import DetDataSvc
    from Configurables import LoKiSvc

    #
    ## some reshuffling of order of services is needed
    #    in particular DOD should come after PPSVC, LoKiSvc and ToolSvc
    #

    services = app.ExtSvc
    app.ExtSvc = [
        DetDataSvc('DetectorDataSvc'),
        PPSvc(),
        LoKiSvc(Welcome=False)
    ] + services

    #
    ## suppress some prints
    #
    from Configurables import TimingAuditor
    timer = TimingAuditor()
    from Configurables import SequencerTimerTool
    timer.addTool(SequencerTimerTool, 'TIMER')
    timer.TIMER.OutputLevel = ERROR

    # suppress printout of various summaries from algorithms.
    from Gaudi.Configuration import allConfigurables
    for conf in allConfigurables.values():
        for opt in ('ErrorsPrint', 'HistoPrint'):
            if opt in conf.__slots__:
                setattr(conf, opt, False)

    # ensure that prints from the main tools/factories are not suppressed
    import Configurables
    from Configurables import LoKi__Hybrid__CoreFactory as CoreFactory
    from Configurables import LoKi__Hybrid__HltFactory as HltFactory
    for Factory, names in {
            HltFactory: ("HltFactory", "Hlt1HltFactory", "Hlt2HltFactory"),
            CoreFactory: ("Hlt1CoreFactory", "Hlt2CoreFactory", "Hlt1Factory")
    }.items():
        for name in names:
            f = Factory(name)
            f.OutputLevel = ERROR


#
## use this action!
#

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(post_action_for_cpp)

# =============================================================================
# The END
# =============================================================================
