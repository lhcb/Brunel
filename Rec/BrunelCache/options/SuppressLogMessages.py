# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from builtins import map
from Gaudi.Configuration import log
from GaudiKernel.Configurable import log as clog
import logging
import re

IGNORED_MESSAGES = list(
    map(
        re.compile,
        (  # errors
            # warnings
            r'Using default tag.*for partition',
            r'something else configured a decoder already',
            r'Property .* is set in both')))


class MessageFilter(logging.Filter):
    def filter(self, record):
        if record.levelno >= logging.WARNING:
            if any(exp.search(record.msg) for exp in IGNORED_MESSAGES):
                return False
            if not record.msg.strip():
                return False  # why should anyone want to print an empty warning?
        return True


for l in (log, clog):
    l.addFilter(MessageFilter())
