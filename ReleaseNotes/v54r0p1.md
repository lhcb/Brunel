
2018-04-06 Brunel v54r0p1
===

This version uses Rec v23r1, Lbcom v22r0p1, LHCb v44r1, Gaudi v29r3 and LCG_93 with ROOT 6.12.06

This version is a production release for 2018 data-taking

This version is released on `2018-patches` branch.

Identical to Brunel v54r0, with updated dependencies and the following changes:

### Monitoring changes
- Increased debug printout from PCPrimaryVertex LHCb!1180, !374 (@adavis, @cattanem)   

### Changes to tests
- Add a test for 2018 DataType. Currently identical to 2017, to be updated when 2018 data available, !378 (@cattanem)   



  
