2017-09-12 Brunel v52r6p1
---
This version uses projects LHCb v42r6p1, Lbcom v20r6p1, Rec v21r6p1, Gaudi v28r2, LCG_88
 (Root 6.08.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*

This version is released on 2017-patches branch

This version is a patch release for the 2017 prompt reconstruction, Reco17


## Bug fixes
**[MR LHCb!873] Fix an out-of-bounds read on a vector in DeRichHPDPanel**  

**[MR Rec!707] Fix MuonID compilation warnings in clang and gcc7**  

**[MR Rec!738] RichFutureRecPixelAlgorithms - Add out of bounds checks**  
Add some additional checks to make sure no out-of-bounds read access to vectors are performed in PixelBackgroundsEstiAvHPD, related to MR LHCb!873  

**[MR Rec!741] Redo MuonID change to use force=True with PrintOn, lost in a previous MR** 
